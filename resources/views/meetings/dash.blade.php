@extends('layouts.app')

@section('content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
            <!-- Card stats -->
            <div class="row">
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">NAME</h5>
                                    <span class="h2 font-weight-bold mb-0">{{auth()->user()->name}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                        <i class="fas fa-chart-bar"></i>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Organization</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$org->name}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                                        <i class="fas fa-chart-pie"></i>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">role</h5>
                                    <span class="h2 font-weight-bold mb-0">{{auth()->user()->role}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                                        <i class="fas fa-users"></i>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">total meeting</h5>
                                    @foreach($totlemeet as $totleemeets)
                                    <span class="h2 font-weight-bold mb-0">{{$totleemeets->total}}</span>
                                    @endforeach
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                                        <i class="fas fa-percent"></i>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-7 mb-5 mb-xl-0">
                  <div class="card shadow">
                    <div class="card-body">
                        <div class="table-responsive">
                            {!! $chart->html() !!}
                         </div>
                    </div>
                    
                </div>
            </div>
            <div class="col-xl-5">
                <div class="card shadow">
                    
                    <div class="card-body">
                        <!-- Chart -->
                        <div class="table-responsive">
                            {!! $pie->html() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-xl-7 mb-5 mb-xl-0">
                <div class="card shadow">
                   
                    <div class="table-responsive">
                        <!-- Projects table -->
                        {!! $donut->html() !!}
                    </div>
                </div>
            </div>
            <div class="col-xl-5">
                <div class="card shadow">
                    
                    <div class="table-responsive">
                        <!-- Projects table -->
                        {!! $exc->html() !!}
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
    {!! Charts::scripts() !!}
    {!! $chart->script() !!}
    {!! $pie->script() !!}
    {!! $donut->script() !!}
    {!! $exc->script() !!}
 
@endsection

