@extends('layouts.app')

@section('content')
@include('layouts.headers.list')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create a new meeting') }}</h3>
                            </div>
                            
                        </div>
                    </div>
                    <div id="box">
                    
                    <div class = "col-md-5">
                    <div class = "container">
                    <form method="post" action="{{ action('MeetingController@store') }}" autocomplete="on">
                            @csrf
                            

                            <div class="pl-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="name">{{ __('Name of the meeting') }}</label>
                                    <input type="text" name="name"  class="form-control form-control-alternative" placeholder="{{ __('Name of the meeting') }}" value="" required>
                                </div>
                                
                                <div class="form-group">
                                    <label class="form-control-label" for="location">{{ __('Location of the meeting') }}</label>
                                    <input type="datetime" name="location"  class="form-control form-control-alternative" placeholder="{{ __('Location of the meeting') }}" value="" required>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="date">{{ __('Date of the meeting') }}</label>
                                    <input type="date" name="date"  class="form-control form-control-alternative" placeholder="{{ __('Date  of the meeting') }}" value="" required>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="hour">{{ __('Hour of the meeting') }}</label>
                                    <input type="time" name="hour"  class="form-control form-control-alternative" placeholder="{{ __('Hour of the meeting') }}" value="" required>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="hend">{{ __('End hour of the meeting') }}</label>
                                    <input type="time" name="hend"  class="form-control form-control-alternative" placeholder="{{ __('End hour of the meeting') }}" value="" required>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="participants">{{ __('Participants') }}</label>
                                    <select class="form-control m-bot15" name="participants[]" class="form-control form-control-alternative" placeholder="{{ __('Participants of the meeting') }}" value="" required>
                                    @foreach($users as $user)   
                                        <option value="{{ $user->id }}" {{ $selectedRole = $user->id ? 'selected="selected"' : '' }}>{{ $user->email }}</option> 
                                    @endforeach
                                    </select>
                                </div>
                                
                                <div id='participants'>

                                </div>

                                <div  id ="addp">
                                <a class="btn btn-info mt-2" >{{ __('Add participants') }}</a>
                                 </div><br>

                                <div class="form-group">
                                    <label class="form-control-label" for="topic_title">{{ __('Topics') }}</label>
                                    <input type="text" name="topic_title[]"  class="form-control form-control-alternative" placeholder="{{ __('Topics of the meeting') }}" value="" required><br>
                                    <input type="time" name="topic_hour[]"  class="form-control form-control-alternative"  value="" required>

                                </div>
                                

                                <div id='topics'>

                                </div>
                                <div  id ="add">
                                <a class="btn btn-info mt-2" >{{ __('Add topic') }}</a>
                                 </div>

                                 @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                 

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4" value="Save">{{ __('Create') }}</button>
                                </div>
                            </div>
                        </form>
                     </div>
                     </div>
                     </div>
                    
                    
                    
                </div>
            </div>
        </div>

        <script>

                function createTicketComponent(type) {
                type = type || null;
                
                var elements   = [],
                    rootElement = document.createElement('p');
                        
                elements.push('<label class="form-control-label" for="topic">{{ __('Topics') }}</label><input type="text" name="topic_title[]"  class="form-control form-control-alternative" placeholder="{{ __('Topics of the meeting') }}" value="" required><br>');
                elements.push('<input type="time" name="topic_hour[]"  class="form-control form-control-alternative"  value="" required><br>');
                
                    
                rootElement.innerHTML = elements.join('');
                
                return rootElement;
                }

                function createTicketComponent2(type) {
                type = type || null;
                
                var elements   = [],
                    rootElement = document.createElement('p');
                        
                elements.push('<label class="form-control-label" for="participants">{{ __('Participants') }}</label>');
                elements.push('<select class="form-control m-bot15" name="participants[]" class="form-control form-control-alternative" placeholder="{{ __('Participants of the meeting') }}" value="" required>@foreach($users as $user)<option value="{{ $user->id }}" {{ $selectedRole = $user->id ? 'selected="selected"' : '' }}>{{ $user->email }}</option> @endforeach</select><br>');
                
                    
                rootElement.innerHTML = elements.join('');
                
                return rootElement;
                }

              


                function onClickCreateTicketButton(event) {
                var button    = event.target,
                    container = document.querySelector('#topics'),
                    component;

                
                    component = createTicketComponent();
                

                container.appendChild(component);
                }

                function onClickCreateTicketButton2(event) {
                var button    = event.target,
                    container = document.querySelector('#participants'),
                    component;

                
                    component = createTicketComponent2();
                

                container.appendChild(component);
                }

                
                var buttonsGroup = document.getElementById('add');
                buttonsGroup.addEventListener('click', onClickCreateTicketButton);
                var buttonsGroup2 = document.getElementById('addp');
                buttonsGroup2.addEventListener('click', onClickCreateTicketButton2);


</script>
            
        @include('layouts.footers.auth')
    </div>
@endsection