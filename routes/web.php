<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/neworg', function () {
    return view('auth.registeror');
})->name('neworg');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('meetings', 'MeetingController')->middleware('auth');


Route::post('meetings/todo/{id}', 'MeetingController@add_todo')->name('meetings.add_todo');
Route::get('meetings/create_todo/{mid}', 'MeetingController@create_todo')->name('meetings.create_todo');


Route::get('meetings/spmeet/{id}', 'MeetingController@spmeet')->name('meetings.spmeet');

Route::get('meetings/dash/{id}', 'MeetingController@dash')->name('meetings.dash');

Route::get('meetings/edittopic/{id}', 'MeetingController@edittopic')->name('meetings.edittopic');
Route::patch('meetings/updatetopic/{id}', 'MeetingController@updatetopic')->name('meetings.updatetopic');

Route::get('meetings/edittodo/{id}', 'MeetingController@edittodo')->name('meetings.edittodo');
Route::patch('meetings/updatetodo/{id}', 'MeetingController@updatetodo')->name('meetings.updatetodo');


Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	Route::put('profile/org', ['as' => 'profile.org', 'uses' => 'ProfileController@org']);
});

