<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $fillable = [
       'user_id',	'meeting_id',
    ];

    public function meetings()
    {
        return $this->belongsTo('App\Meeting');
    }
}
