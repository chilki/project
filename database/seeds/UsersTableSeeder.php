<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'Admin Admin',
                    'email' => 'a@a.com',
                    'role' => 'admin',
                    'organization_id' => '1',
                    'email_verified_at' => now(),
                    'password' => Hash::make('123456789'),
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'name' => 'Rony',
                    'email' => 'b@b.com',
                    'role' => 'manager',
                    'organization_id' => '1',
                    'email_verified_at' => now(),
                    'password' => Hash::make('123456789'),
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'name' => 'Yarden',
                    'email' => 'c@c.com',
                    'role' => 'employee',
                    'organization_id' => '1',
                    'email_verified_at' => now(),
                    'password' => Hash::make('123456789'),
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'name' => 'fsdfd',
                    'email' => 'd@d.com',
                    'role' => 'admin',
                    'organization_id' => '2',
                    'email_verified_at' => now(),
                    'password' => Hash::make('123456789'),
                    'created_at' => now(),
                    'updated_at' => now()
                ],
            ]);
    }
}
