<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate; 
use App\Meeting;
use App\Topic;
use App\Participant;
use App\Todo;
use App\Organization;
use Charts; 
class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('admin')){
            $id= Auth::id();
            $user2 = User::find($id);
            $numorg = $user2->organization_id;
            $meetings2 = DB::table('meetings')
                ->join('users', 'meetings.user_id', '=', 'users.id')
                ->select('meetings.*')->where('users.organization_id',$numorg)
                ;
            $meetings = DB::table('meetings')
                ->join('participants', 'meetings.id', '=', 'participants.meeting_id')
                ->select('meetings.*')->where('participants.user_id',$id)
                ->union($meetings2)
                ->get();
                return view('meetings.index',['meetings'=>$meetings]);
        }
        if(Gate::allows('employee') || Gate::allows('manager')){
            $id= Auth::id();
            $meetings = DB::table('meetings')
                ->join('participants', 'meetings.id', '=', 'participants.meeting_id')
                ->select('meetings.*')->where('participants.user_id',$id)
                ->get();
            return view('meetings.index',['meetings'=>$meetings]);    
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        if(Gate::allows('admin') || Gate::allows('manager')){
            $id= Auth::id();
            $user = DB::table('users')->where('id','!=',$id)->get();

            return view('meetings.create',['users'=>$user]);
        }else{
            abort(403,'Sorry you are not allow to crate meetings...');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Gate::denies('admin') || Gate::denies('manager')){
            $id= Auth::id();
            $user2 = User::find($id);
            $numorg = $user2->organization_id;
            $mintopic = DB::table('organizations')->where('id', $numorg)->value('mintopic');
            $this->validate($request,[
                'topic_title' =>"required|array|min:$mintopic",
                'hend' => "after:hour",
                'topic_hour.*' => "after:hour|before:hend",
                'date' => "after:today",
            ]);
            
            
            $meeting = new Meeting();
            $id = Auth::id();
            $meeting->title = $request->name;
            $meeting->user_id = $id;
            $meeting->location = $request->location;
            $meeting->date = $request->date;
            $meeting->hour = $request->hour;
            $meeting->hend = $request->hend;
            $meeting->save();
            
            $mid = $meeting->id;
            $topic_title = $request->input('topic_title');
            $topic_hour = $request->input('topic_hour');
            for ($i=0; $i <count($topic_title) ; $i++) { 
                $topic = new Topic();
                $topic->topic_title = $topic_title[$i];
                $topic->topic_hour = $topic_hour[$i];
                $topic->meeting_id = $mid;
                $topic->status = 0;
                $topic->save();
            }
            $part = new Participant();
            $part->user_id =$id;
            $part->meeting_id = $mid;
            $part->save();  

            $participants = $request->input('participants');
            for ($i=0; $i < count($participants) ; $i++) { 
                $participant = new Participant();
                $participant->user_id = $participants[$i];
                $participant->meeting_id = $mid;
                $participant->save();
            }
        
            
            return redirect('meetings');

        }else{
            abort(403,'Are you hacker or what?');
        }
       
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('admin') || Gate::allows('manager')){
            $meeting = Meeting::find($id);
            return view('meetings.edit',compact('meeting'));
        }else{
            abort(403,'Are you hacker or what?');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Gate::allows('admin') || Gate::allows('manager')){
            $meeting = Meeting::find($id);
            $this->validate($request,[
                'hend' => "after:hour",
                'date' => "after:today",
            ]);
            $meeting->update($request->all());    
            return redirect('meetings');
        }else{
            abort(403,'Are you hacker or what?');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::allows('admin') || Gate::allows('manager')){
            $meeting = Meeting::find($id);
            $meeting ->delete();
            
            $meeting_id = $id;
            $topic = Topic::where('meeting_id',$id);
                $topic ->delete();
            $participant = Participant::where('meeting_id',$id);
                $participant ->delete(); 
            
            return redirect('meetings');
        }else{
            abort(403,'Are you hacker or what?');
        }
    }

    public function create_todo($mid)
    {
        if(Gate::allows('admin') || Gate::allows('manager')){
            $part = DB::table('participants')
            ->join('users', 'participants.user_id', '=', 'users.id')
            ->select('users.name','users.id','participants.meeting_id')->where('participants.meeting_id',$mid)
            ->get();
            return view('meetings.createtodo',['parts'=>$part]);
        }else{
            abort(403,'Sorry you are not allow to crate todos...');
        }

    }

    public function add_todo($mid,Request $request)
    {
        if(Gate::allows('admin') || Gate::allows('manager')){
            $this->validate($request,[
                'due_date' => "after:today",
            ]);
            $todo = new Todo;
            $todo->title = $request->title;
            $todo->status = 0; 
            $todo->user_id = $request->user_id;
            $todo->meeting_id = $mid;
            $todo->due_date = $request->due_date;
            $todo->save();
            return redirect('meetings');
        }else{
            abort(403,'Are you hacker or what?');
        }

    }

    public function spmeet($mid,Request $request)
    {
        
        //$topics = DB::table('topics')->where('meeting_id', $mid)->get(); 
        $id= Auth::id();
        $topics = DB::table('meetings')
            ->join('topics', 'meetings.id', '=', 'topics.meeting_id')
            ->select('topics.*','meetings.hour','meetings.hend','meetings.date')->where('topics.meeting_id',$mid)
            ->get();
        if(Gate::allows('employee')){
            $todos = DB::table('todos')->where([
                ['meeting_id', $mid],
                ['user_id',$id]
                ])->get();
        }else{
            $todos = DB::table('todos')->where('meeting_id', $mid)->get();
        }
       

        return view('meetings.spmeet',['topics'=>$topics,'todos'=>$todos]);
    }

    public function edittopic($id)
    {
        if(Gate::allows('admin') || Gate::allows('manager')){
            $topic = Topic::find($id);
            $id= Auth::id();
            $allmeeting = DB::table('meetings')->where('user_id', $id)->get(); 
            return view('meetings.edittopic',compact('topic',"allmeeting"));
        }else{
            abort(403,'Are you hacker or what?');
        }
    }

    public function updatetopic(Request $request, $id)
    {
        if(Gate::allows('admin') || Gate::allows('manager')){
            
            $topic = Topic::find($id);
            $time = DB::table('topics')
            ->join('meetings','topics.meeting_id','=','meetings.id')
            ->select('meetings.hour','meetings.hend')
            ->where('topics.id',$id)->get();
            foreach ($time as $times) {
                $sh = $times->hour;
                $eh = $times->hend;
            };
            //die($eh);
            $this->validate($request,[
                'topic_hour' => "after:$sh|before:$eh",
                
            ]);
               
            $topic->update($request->all());
            $topic->update($request ->except(['_token']));
            if($request->ajax()){
                return Response::json(array('result'=>'success','status'=>$request->status),200);    
               }    
            return redirect('meetings');
        }else{
            abort(403,'Are you hacker or what?');
        }
    }

    public function edittodo($id)
    {
        if(Gate::allows('admin') || Gate::allows('manager')){
            $todo = Todo::find($id);
            return view('meetings.edittodo',compact('todo'));
        }else{
            abort(403,'Are you hacker or what?');
        }
    }

    public function updatetodo(Request $request, $id)
    {
        if(Gate::allows('admin') || Gate::allows('manager')){
            $this->validate($request,[
                'due_date' => "after:today",
            ]);
            $todo = Todo::find($id);
            $todo ->update($request->all());   
            $todo->update($request ->except(['_token']));
            if($request->ajax()){
                return Response::json(array('result'=>'success','status'=>$request->status),200);    
            }  
            return redirect('meetings');
        }elseif(Gate::allows('employee')){
                $todo = Todo::find($id);
                
                $todo->update($request ->except(['_token']));
                if($request->ajax()){
                    return Response::json(array('result'=>'success','status'=>$request->status),200);    
                } 
        }else{
            abort(403,'Are you hacker or what?');
        }
    }
    
    public function dash($id)
    {
        $id= Auth::id();
        $user2 = User::find($id);
        $numorg = $user2->organization_id;
        $org = Organization::find($numorg);
        $totlemeet = DB::table('meetings')
            ->join('participants', 'meetings.id', '=', 'participants.meeting_id')
            ->select(DB::raw("(count(meetings.id))as total"))
            ->where('participants.user_id',$id)->get();
        $numofmeet = DB::table('meetings')
            ->join('participants', 'meetings.id', '=', 'participants.meeting_id')
            ->select(DB::raw("MONTH(date) as date"),DB::raw("(count(meetings.id))as total"))
            ->where('participants.user_id',$id)
            ->groupBy(DB::raw("MONTH(date)"))->get();
        //die($numofmeet);
        $chart = Charts::create('bar', 'highcharts')
			      ->title("Number of meeting per month new ")
			      ->elementLabel("Total Meeting")
                  ->dimensions(1000, 500)
                  ->labels($numofmeet->pluck('date'))
                  ->values($numofmeet->pluck('total'))
			      ->responsive(true);
                  
        $todo = DB::table('todos')->select('status',DB::raw("(count(id))as total"))
        ->where('user_id',$id)
        ->groupBy('status')->get();
       
        //die($todo);
        $pie = Charts::create('pie', 'highcharts')
			      ->title("Number of todo that completed ")
			      ->elementLabel("Total complete")
                  ->dimensions(1000, 500)
                  ->labels($todo->pluck('status'))
                  ->values($todo->pluck('total'))
			      ->responsive(true);
        
        $numoftodoformeet = DB::table('meetings')
            ->join('todos', 'meetings.id', '=', 'todos.meeting_id')
            ->select('meetings.title',DB::raw("(count(todos.id))as total"))->where('todos.user_id',$id)
            ->groupBy('meetings.title')
            ->get();
       
        //die($numoftodoformeet);
        $donut = Charts::create('donut', 'highcharts')
			      ->title("Number of todo per meeting ")
			      ->elementLabel("Total complete")
                  ->dimensions(1000, 500)
                  ->labels($numoftodoformeet->pluck('title'))
                  ->values($numoftodoformeet->pluck('total'))
                  ->responsive(true);
                  
        $excelleantworker = DB::table('todos')
                  ->join('users', 'users.id', '=', 'todos.user_id')
                  ->select('users.name',DB::raw("(count(todos.id))as total"))->where('status', '>',0)
                  ->groupBy('users.name')
                  ->orderBy('total','desc')
                  ->get();
             
        //die($excelleantworker);
        $exc = Charts::create('bar', 'highcharts')
                    ->title("Excellent worker ")
                    ->elementLabel("Total todo complete")
                    ->dimensions(1000, 500)
                    ->labels($excelleantworker->pluck('name'))
                    ->values($excelleantworker->pluck('total'))
                    ->responsive(true);
        
        return view('meetings.dash',compact('chart','pie','donut','exc','org','totlemeet'));
    }

}
