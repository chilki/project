<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Support\Facades\Hash;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate; 
use App\Meeting;
use App\Topic;
use App\Participant;
use App\Todo;
use App\Organization;
use Charts; 

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        $id= Auth::id();
        $user2 = User::find($id);
        $numorg = $user2->organization_id;
        $org = Organization::find($numorg);
        return view('profile.edit',compact('org'));
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request)
    {
        auth()->user()->update($request->all());

        return back()->withStatus(__('Profile successfully updated.'));
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        return back()->withPasswordStatus(__('Password successfully updated.'));
    }

    public function org(Request $request)
    {
        $id= Auth::id();
        $user2 = User::find($id);
        $numorg = $user2->organization_id;
        $org = Organization::find($numorg);
        $org->update($request->all()); 
        return back()->withStatus(__('Num of topic successfully updated.'));
    }
}
