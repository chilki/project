@extends('layouts.app')

@section('content')
@include('layouts.headers.list')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create a new Todo') }}</h3>
                            </div>
                            
                        </div>
                    </div>
                    
                    
                    <div class = "col-md-5">
                    <div class = "container">
                    <form method="post" action="{{route('meetings.add_todo', Request::segment(3)  )}}" autocomplete="on">
                            @csrf
                            

                            <div class="pl-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="title">{{ __('Name of the todo') }}</label>
                                    <input type="text" name="title"  class="form-control form-control-alternative" placeholder="{{ __('Name of the todo') }}" value="" required>
                                </div>
                            </div>

                            <div class="pl-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label" for="due_date">{{ __('Due Date') }}</label>
                                    <input type="date" name="due_date"  class="form-control form-control-alternative" placeholder="{{ __('Due Date') }}" value="" required>
                                </div>
                            </div>

                            <div class="pl-lg-4">
                            <div class="form-group">
                                    <label class="form-control-label" for="user_id">{{ __('Participants') }}</label>
                                    <select class="form-control m-bot15" name="user_id" class="form-control form-control-alternative" placeholder="{{ __('User to perform') }}" value="" required>
                                    @foreach($parts as $part)   
                                        <option value="{{ $part->id }}" {{ $selectedRole = $part->id ? 'selected="selected"' : '' }}>{{ $part->name }}</option> 
                                    @endforeach
                                    </select>
                            </div>
                            </div>

                            @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                            <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4" value="Save">{{ __('Add') }}</button>
                            </div>
                                
                        </form>
                     </div>
                     </div>
                     
                    
                    
                    
                </div>
            </div>
        </div>
         
        @include('layouts.footers.auth')
    </div>
@endsection