<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{

    protected $fillable = [
        'title', 'location', 'date', 'hour',

    ];
    protected $casts = [
        'hour' => 'hh:mm',
        'hend' => 'hh:mm',
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function participants()
    {
        return $this->hasMany('App\Participant');
    }

}
