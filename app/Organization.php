<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',	'name', 'mintopic',
     ];
}
