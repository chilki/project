@extends('layouts.app')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@section('content')
@include('layouts.headers.list')
<style>
            @-webkit-keyframes invalid {
        from { background-color: red; }
        to { background-color: inherit; }
        }


        .invalid {
        -webkit-animation: invalid 1s infinite; /* Safari 4+ */
        -moz-animation:    invalid 1s infinite; /* Fx 5+ */
        -o-animation:      invalid 1s infinite; /* Opera 12+ */
        animation:         invalid 1s infinite; /* IE 10+ */
        }

</style>

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Topics') }}</h3>
                            </div>
                           
                        </div>
                    </div>
                    
                  

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                @php $ndate = new DateTime('now');
                                    $nowdate = $ndate->format('Y-m-d');
                                 @endphp
                                @foreach ($topics as $topic)
                                
                                    @if( strtotime($topic->hour) < time() && strtotime($topic->hend) > time() && (strtotime($topic->date) == strtotime($nowdate)) )
                                        <th scope="col">{{ __('Dis') }}</th>
                                        @break
                                    @endif
                                @endforeach
                                    <th scope="col">{{ __('Name') }}</th>
                                    <th scope="col">{{ __('Hour') }}</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($topics as $topic)
                                    <tr>
                                  
                                        @if( strtotime($topic->hour) < time() && strtotime($topic->hend) > time() && (strtotime($topic->date) == strtotime($nowdate)) )
                                        <td>
                                            @if ($topic->status)
                                                <input type = 'checkbox' class="top" id ="{{$topic->id}}" checked>
                                            @else
                                                <input type = 'checkbox' class="top" id ="{{$topic->id}}">
                                            @endif
                                        </td>
                                        @endif
                                        <td>@cannot('employee')<a href = "{{route('meetings.edittopic',$topic->id )}}"">@endcannot{{ $topic->topic_title }}</td>
                                        <td>{{ $topic->topic_hour }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Todos') }}</h3>
                            </div>
                           
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Finish') }}</th>
                                    <th scope="col">{{ __('Name') }}</th>
                                   
                                    <th scope="col">{{ __('Due Date') }}</th>
                                    
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($todos as $todo)

                                    @if (strtotime($todo->due_date) < strtotime('now') && (!$todo->status))
                                        @php $bold = 'invalid' @endphp
                                    @else 
                                        @php $bold = '' @endphp
                                    @endif

                                    <tr class = "{{ $bold }}">
                                    <td>
                                            @if ($todo->status)
                                                <input type = 'checkbox' class ="tod" id ="{{$todo->id}}" checked>
                                            @else
                                                <input type = 'checkbox' class ="tod" id ="{{$todo->id}}">
                                            @endif
                                        </td>
                                        <td>@cannot('employee')<a href = "{{route('meetings.edittodo',$todo->id )}}"">@endcannot{{ $todo->title }}</td>
                                        
                                        <td>{{ $todo->due_date }}</td>
                                       
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
    
<script>
       $(document).ready(function(){
           $(".top").click(function(event){
            console.log(event.target.id)
               $.ajax({                  
                   url:  "{{url('meetings/updatetopic')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'patch' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               
               });  
                         
           });
       });

       $(document).ready(function(){
           $(".tod").click(function(event){
            console.log(event.target.id)
               $.ajax({                  
                   url:  "{{url('meetings/updatetodo')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'patch' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               
               });  
                         
           });
       });
   </script> 
@endsection